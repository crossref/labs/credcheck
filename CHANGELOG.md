# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.3] - 2024-04-13
* Catch for incorrect login (authentication) and varying JSON responses from authenticator

## [0.0.2] - 2024-04-13
* Add a catch to authorisation check for when the login (authentication) is incorrect.